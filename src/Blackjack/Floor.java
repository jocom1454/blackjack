package Blackjack;

import Cards.*;
import HashTable.*;

import java.util.ArrayList;
import java.util.Hashtable;

public class Floor {
    public Deck deck;
    public ArrayList dealersCards;
    public Hashtable<Player, Integer> playerInfo;
//    public ArrayList playersTotals;
    public ArrayList<Player> players;


    public Floor(Deck deck1, ArrayList cards, Hashtable<Player, Integer> info){
        deck = deck1;
        dealersCards = cards;
        playerInfo = info;
    }


}
