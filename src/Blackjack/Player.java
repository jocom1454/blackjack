package Blackjack;

import Cards.*;

public interface Player {

    boolean choice();

    void setStrategy(Strategy s);

    int getID();

}
