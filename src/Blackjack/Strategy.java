package Blackjack;

import Cards.*;

public interface Strategy {

    boolean makeChoice(Hand h, Floor f);

    void updateData();


}
