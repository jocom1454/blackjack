/*
Student Number - 100211671
Author - ukk17cbu
This is the deck class for the Whilst game. This is a collection of cards.
This is a serialised class.
 */
package Cards;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Deck implements Serializable, Iterable<Card>{

    //This declares the attributes
    private static final long serialVersionUID = 49L;
    //This is declaring the array of cards and limiting it to a number of 52
    List<Card> deck = new ArrayList(Arrays.asList(new Card[52]));
    //This is declaring the deck of Spades that will be filled using the iterator
    List<Card> deckOfSpades = new ArrayList(Arrays.asList(new Card[0]));
    int i = 0;

    //This is the Constructor
    public Deck() {
        //This is a for loop that will loop through all the values of the Suit enum from the Card class
        for (Card.Suit suit : Card.Suit.values()) {
            //This is a for loop that will loop through all the values of the Rank enum from the Card class
            for (Card.Rank rank : Card.Rank.values()) {
                //This creates all the cards and adds them to the Deck
                deck.set(i++, new Card(rank, suit));
            }
        }
        //This shuffles the deck
        Collections.shuffle(deck);
    }

    //This is an accessor
    public List<Card> getDeck() {
        return this.deck;
    }
    //This is an accessor
    public List<Card> getDeckOfSpades() {
        return this.deckOfSpades;
    }

    //This is a method that will return the current size of the deck
    public int size() {
        return this.deck.size();
    }

    public int size1() {
        return this.deckOfSpades.size();
    }

    //This method will reinitialise the current deck and refill it completely
    public final void newDeck() {
        //This will clear the current deck
        deck.clear();
        //This is a for loop that will loop through all the values of the Suit enum from the Card class
        for (Card.Suit suit : Card.Suit.values()) {
            //This is a for loop that will loop through all the values of the Rank enum from the Card class
            for (Card.Rank rank : Card.Rank.values()) {
                Card why = new Card(rank, suit);
                //This creates all the cards and adds them to the Deck
                deck.add(why);
            }
        }
        //This shuffles the deck
        Collections.shuffle(deck);
    }

    //This is an iterator that will traverse the deck in the order it is to be dealt
    private class DeckIterator<Card> implements Iterator<Card> {

        //This is a declaration of the variable that will keep track of the position
        int pos = 0;

        //This checks if there is a card after it
        @Override
        public boolean hasNext() {
            return pos < deck.size();
        }

        @Override
        //If there is a card after the current card, it will return the current card
        public Card next() {
            return (Card) deck.get(pos++);
        }
    }

    //This is a delcaration of the iterator class
    @Override
    public Iterator<Card> iterator() {
        //This returns the deck iterator
        return new DeckIterator();
    }

    //This is a delcaration of the iterator class
    public Iterator<Card> spadeIterator() {
        //This returns the deck iterator
        return new SpadeIterator();
    }


    //This will use the iterator to deal the first card
    public Card deal(){
        Iterator iterator = deck.iterator();
        if(!iterator.hasNext()){
            throw new IllegalStateException("No cards are left");
        }
        Card dealCard = (Card) iterator.next();
        deck.remove(dealCard);
        return dealCard;
    }

    //This is a method that will be called in the SpadeIterator
    public List<Card> getSpadeIterator(){
        //For each card in the deck
        for (int i = 0; i < deck.size(); i++) {
            //If the card's suit is a SPADES
            if (deck.get(i).suit.toString().contains("SPADES")) {
                //Add it to the deckOfSpades deck
                deckOfSpades.add(deck.get(i));
            }
        }
        //Then return the deckOf Spades
        return deckOfSpades;
    }

    //This is the nested class spade iterator
    private class SpadeIterator<Card> implements Iterator<Card> {

        //This is a declaration of the variable that will keep track of the position
        int pos = 0;
        //This calls the method above to add cards to the deckOfSpades deck
        public SpadeIterator(){
            getSpadeIterator();
        }

        @Override
        public boolean hasNext() {
            //This will return the card at pos until it reaches the end of the ArrayList
            return pos < deckOfSpades.size();

        }

        @Override
        public Card next() {
            //This returns the card in that particular position
            return (Card) deckOfSpades.get(pos++);
        }

    }

    public static void main(String[] args) {
        //This is creating a new deck object called deck1
        Deck deck1 = new Deck();
        //This is calling the new deck method
        deck1.newDeck();

        //This is printing out the current amount of cards in the deck
        System.out.println(deck1.size());
        //This is a for loop that will iterate through the deck using the iterator
        for (Iterator<Card> itr = deck1.iterator(); itr.hasNext();) {
//            //This prints the current card
            System.out.println(itr.next());
        }

        //This is a for loop that will loop through the deckOfSpades using the iterator
        //for (Iterator<Card> itr = deck1.spadeIterator(); itr.hasNext();) {
        //This prints the current card
        //System.out.println(itr.next());
        //}

        //This will run the deal method and print the first card
//        System.out.println(deck1.deal());

        //This is a for loop that will iterate through the entire deck and print it
        //for(int i = 0; i < deck1.size(); i++){
        //This should return null for the first value and the rest of the cards
        //System.out.println(deck1.deck.get(i));
        //}

        //This prints the deck size after deal has been run
//        System.out.println(deck1.size());
    }

}
