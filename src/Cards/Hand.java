/*
Student Number - 100211671
Author - ukk17cbu
This is the hand class for the Whilst game. This is a collection of cards.
This is a serialised class.
 */
package Cards;

import java.util.ArrayList;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import Cards.Card.CompareRank;
import Cards.Card.Rank;
import Cards.Card.Suit;

public class Hand implements Serializable, Iterable<Card> {

    //This is a decllaration of all the attributes
    private static final long serialVersionUID = 300L;
    //This is a list of cards that will act as the hand
    public List<Card> hand1;
    ArrayList<Integer> handValues = new ArrayList<>();

    //This is a constructor that creates an empty hand
    public Hand() {
        this.hand1 = new ArrayList<>();

    }

    //This will add an array list of cards into the current hand
    public Hand(Card[] hand) {
        this();
        this.hand1.addAll(Arrays.asList(hand));
        handValues();
    }

    //This will add all the cards from one hand to another hand
    public Hand(Hand hand) {
        this();
        this.hand1.addAll(hand.hand1);
        handValues();
    }

    //This is an add method that will add a single card to the hand
    public void add(Card card) {
        hand1.add(card);
        handValues();
    }

    //This is an add method that will add a collection of cards to the hand
    public void add(Collection<Card> card) {
        this.hand1.addAll(card);
        handValues();
    }

    //Thi is an add method that will add all the cards from one hand to another hand
    public void add(Hand hand) {
        this.hand1.addAll(hand1);
        handValues();
    }

    //This is a remove method that will remove a single card
    public boolean remove(Card card) {
        //This is creating a boolean result that will tell you if removing the card has been sucessful
        boolean contain = hand1.contains(card);
        if (contain) {
            hand1.remove(card);
            handValues();
        }
        return contain;
    }

    //This is a remove method that will remove all the cards from any given hand
    public boolean remove(Hand hand) {

        boolean result = true;
        for (Card c : hand.hand1) {
            if (!hand.remove(c)) {
                result = false;
                handValues();
            }
        }
        return result;
    }

    //This is a remove method that will remove a card at a particular position
    public Card remove(int i) {
        //This assigns the card that will be removed to a card called c
        Card c = hand1.remove(i);
        //This will then return the card c and run the method handValues()
        handValues();
        return c;
    }

    //This is a method that will determine the value of the current hand
    public void handValues() {
        int aceValue = 0;
        int maxValue = 0;

        //This is a loop that will iterate through the hand
        for (Card hand12 : hand1) {
            //The max value will calculate the value of the hand if the aces are 11
            maxValue += hand12.getRank().value;
            //This will also count the amount of aces in the hand and increment the aceValue variable
            if (hand12.getRank() == Card.Rank.ACE) {
                aceValue++;
            }
        }
        //This will then clear the handValues array
        handValues.clear();
        //This will also increment the size of array based on the amount of aces
        for (int i = 0; i <= aceValue; i++) {
            handValues.add(0);
        }

        //This sets the first value to the maxValue
        handValues.set(aceValue, maxValue);
        //This will then minus 10 from the maxValue until the array is full
        for (int i = aceValue - 1; i >= 0; i--) {
            handValues.set(i, handValues.get(i + 1) - 10);
        }
    }

    //This is an iterator method that will allow the hand to be traversed in the order they have been added
    @Override
    public Iterator<Card> iterator() {
        return hand1.iterator();
    }

    //This is a toString that will call the toString for each card from the Card class
    public String toString() {
        //This is a new StringBuilder object that will allow us to create a toString for a hand
        StringBuilder str = new StringBuilder();
        //This will iterate through the hand
        for (Card c : this.hand1) {
            //This will call the toString form Card that will fashion how the hand is to be printed
            str.append(c.toString());
            //This is just adding a new line after each iteration
            str.append("\n");
        }
        //This will return the actual toString object created
        return str.toString();
    }

    //This is a sort method that will sort the cards into ascending order
    public void sort() {
        Collections.sort(hand1);
    }

    //This method will compare the cards by rank
    public void sortByRank() {
        //This will use the Collections.sort method but will also call the CompareRank() method from the Card class
        Collections.sort(hand1, new CompareRank());
    }

    //This is a method that will count the number of a certain suit in each hand
    public int countSuit(Card.Suit suit) {
        //This is a variable that will be incremented
        int difference = 0;
        //This will iterate through the hand
        for (int i = 0; i < hand1.size(); i++) {
            //This is an if loop that checks that the suit entered is the same as the suit on the card
            if (hand1.get(i).getSuit() == suit) {
                //If the suit is the same, it will increment the difference variable
                difference++;
            }
        }
        //This then returns the difference variable
        return difference;
    }

    //This is a method that will count the number of a certain rank in each hand
    public int countRank(Card.Rank rank) {
        //This is a variable that will be incremented
        int difference = 0;
        //This will iterate through the hand
        for (int i = 0; i < hand1.size(); i++) {
            //This is an if loop that checks that the rank entered is the same as the rank on the card
            if (hand1.get(i).getRank() == rank) {
                //If the rank is the same, the difference variable will be incremented
                difference++;
            }
        }
        //It will then return the difference variable
        return difference;
    }

    //This is a boolean method that will check if the current hand has a selected suit
    public boolean hasSuit(Suit suit) {
        //This will iterate through the hand
        for (Card card : hand1) {
            //If the selected suit is in the hand then it will
            if (card.getSuit() == suit) {
                //return true
                return true;
            }
        }
        //Otherwise, it will return false
        return false;
    }

    public static void main(String[] args) {
//        Card[] card1 = {new Card(Rank.ACE, Suit.HEARTS),
//            new Card(Rank.QUEEN, Suit.DIAMONDS),
//            new Card(Rank.TWO, Suit.SPADES),
//            new Card(Rank.JACK, Suit.CLUBS)};
//
//
//
//        Card jigga = new Card(Rank.FIVE, Suit.DIAMONDS);
//
//        Hand newDeck = new Hand(card1);
//        Hand secondDeck = new Hand(newDeck);
//        newDeck.add(jigga);
//        newDeck.sort();
//
//        System.out.println(newDeck.countSuit(Card.Suit.HEARTS));
//        System.out.println(secondDeck.countRank(Rank.KING));
//        System.out.println(secondDeck.hasSuit(Suit.CLUBS));
//
//        System.out.println(secondDeck.toString());
//        System.out.println(newDeck);
//
//        System.out.println(newDeck.handValues);

    }

}
